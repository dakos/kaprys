<!DOCTYPE html>
  <html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <title><?php print $head_title; ?></title>
  <meta charset="utf-8" />
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php
  if (theme_get_setting('kaprys_layout_activate') == 1):
    $stylesheet = array();
	$stylesheet[] = '#wrapper {width:'.intval(theme_get_setting('kaprys_width_total')).'px;}';
	  
	$stylesheet[] = '#left {width:'.intval(theme_get_setting('kaprys_width_left')).'px;}';
	$stylesheet[] = '#left .menu li a {width:'.(intval(theme_get_setting('kaprys_width_left'))-12).'px;}';
	  
	if(theme_get_setting('kaprys_right_sidebar') != 0){
      $stylesheet[] = '#right, .region-sidebar-second {width:'.intval(theme_get_setting('kaprys_width_right')).'px;}';
	  $stylesheet[] = '#right .menu li a {width:'.(intval(theme_get_setting('kaprys_width_right'))-12).'px;}';
	  
	  $stylesheet[] = '#main {width:'.(intval(theme_get_setting('kaprys_width_total'))-(intval(theme_get_setting('kaprys_width_left'))+intval(theme_get_setting('kaprys_width_right'))+21)).'px;
	  margin-left:'.(intval(theme_get_setting('kaprys_width_left'))-1).'px;}';
	  } else {
      $stylesheet[] = '#main {width:'.(intval(theme_get_setting('kaprys_width_total'))-(intval(theme_get_setting('kaprys_width_left'))+21)).'px;
	  margin-left:'.(intval(theme_get_setting('kaprys_width_left'))-1).'px;}';
	  }
    $stylesheet = implode($stylesheet, PHP_EOL);
    echo '<style type="text/css">'.$stylesheet.'</style>';
  endif;
  ?>
  </head>
<body id="top">
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>
