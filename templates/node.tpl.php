<div class="article">
    <?php if ($title && !$page): ?><div class="header">
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2></div>
    <?php endif; ?>
  <?php if ($display_submitted): ?>
    <div class="info"><?php /* todo: zmienic stylowanie na mniejsze literki  */  print $submitted; ?></div>
  <?php endif; ?>
  <div class="article-content">
      <?php
        hide($content['comments']);
        hide($content['links']);
        print render($content);
      ?>
  </div>
  <?php if (!empty($content['links'])): ?>
    <p class="links"><?php print render($content['links']); ?></p>
  <?php endif; ?>
</div>