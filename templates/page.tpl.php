<div id="wrapper">
<?php print render($page['highlighted']); ?>
      <div id="header">
        <div class="inside"><?php if(theme_get_setting('kaprys_header_activate') == 0): print render($page['header']); else: ?>
			<a href="http://zhp.pl"><img src="<?php if(theme_get_setting('kaprys_logotype') == ''): print url($directory.'/css/images/logotype.png'); else: print theme_get_setting('kaprys_logotype'); endif;?>" class="zhp_baner" alt="Związek Harcerstwa Polskiego" /></a>
			<div class="right_side">
				<?php if(theme_get_setting('kaprys_contact_data')): ?><div class="top_menu">
					<?php print $top_menu; ?>
					</div><?php endif; ?>
			<?php print render($page['header']); ?>
			<?php print theme_get_setting('kaprys_contact_data'); ?>
          </div>
		  <?php endif; ?>
          <a href="<?php echo check_url($front_page); ?>"><?php print '<img src="'.$logo.'" alt="'.$site_name.'"/>';?></a></div>
        </div>
		<div class="clear"></div>
      <div id="container">
        <div id="left">
          <div class="inside">
            <div class="block" id="left_menu">
                <?php print $left_menu; ?>
				<?php print render($page['sidebar_first']); ?>
            </div>
          </div>
        </div>
		<?php if(theme_get_setting('kaprys_right_sidebar') != 0 || theme_get_setting('kaprys_layout_activate') == 0): ?><div id="right">
          <div class="inside">
            <div class="block" id="right_link">
                <?php print render($page['sidebar_second']); ?>
            </div><br />
          </div>
        </div><?php endif; ?>
        <div id="main">
          <div class="inside"><?php if ($title): ?>
<header>
            
                <?php print render($title_prefix); ?>
              <h1 id="page-title"><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>

            
              
            
          </header><?php endif; ?>

        <div id="content"><?php print render($page['content']); ?></div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div id="footer">
        <div class="inside">
		<?php if (theme_get_setting('kaprys_footer_activate') == 0): print render($page['footer']); else: ?>
          <div class="footer-left">
              © 1997 - 2012 Związek Harcerstwa Polskiego
          </div>
            <img id="wosm-wagggs-img" src="<?php print url($directory.'/css/images/waggs_wosm.png'); ?>" usemap="#wosm-wagggs" width="273" height="55" alt="" />
<map id="wosm-wagggs" name="wosm-wagggs">
<area shape="rect" coords="2,13,152,50" href="http://scout.org" alt="WOSM" title="WOSM"    />
<area shape="rect" coords="152,9,268,50" href="http://waggs.org" alt="WAGGGS" title="WAGGGS"    />
</map>
          <div class="footer-right"><a href="http://wnt.zhp.pl/"><img src='<?php print url($directory.'/css/images/wnt.png');?>' alt="Wydział Nowych Technologii GK ZHP" title="Wydział Nowych Technologii GK ZHP" width="20" /></a></div><?php endif; ?>
        </div>
      </div>
    </div>