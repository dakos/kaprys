<?php

function kaprys_form_system_theme_settings_alter(&$form, &$form_state) {

//LAYOUT settings
$form['kaprys_layout_activate'] = array(

    '#type' 		 => 'checkbox', 
    '#title'         => t('Activate addional layout settings'),
    '#default_value' => theme_get_setting('kaprys_layout_activate'),
    '#description'   => t('Disable it, if you want to do major modifications in theme'),

);

if(theme_get_setting('kaprys_layout_activate') == 1) {
	$form['kaprys_layout'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Addional layout settings'),
  );
  
	$form['kaprys_layout']['kaprys_right_sidebar'] = array(
    '#type'          => 'radios',
    '#title'         => t('Variant of right sidebar'),
	'#options'       => array(t('None'), t('Normal'), t('Separated')),
    '#default_value' => theme_get_setting('kaprys_right_sidebar'),
    '#description'   => t('Select how should be generated the right sidebar'),
  );
    
  $form['kaprys_layout']['kaprys_width_total'] = array(
    '#type'          => 'textfield',
	'#size'     	 => '5',
	'#maxlength'	 => '4',
    '#title'         => t('Total width of wrapper'),
    '#default_value' => theme_get_setting('kaprys_width_total'),
    '#description'   => t('Enter total width of wrapper (px)'),
	'#element_validate'=> array('_kaprys_width_total_validate'),
  );
  
  	$form['kaprys_layout']['kaprys_width_left'] = array(
    '#type'          => 'textfield',
	'#size'     	 => '5',
	'#maxlength'	 => '4',
    '#title'         => t('Width of left sidebar'),
    '#default_value' => theme_get_setting('kaprys_width_left'),
    '#description'   => t('Enter width of left sidebar (px)'),
	'#element_validate'=> array('_kaprys_width_left_validate'),
  );
  
  if(theme_get_setting('kaprys_right_sidebar') != 0) {
  $form['kaprys_layout']['kaprys_width_right'] = array(
    '#type'          => 'textfield',
	'#size'     	 => '5',
	'#maxlength'	 => '4',
    '#title'         => t('Width of right sidebar'),
    '#default_value' => theme_get_setting('kaprys_width_right'),
    '#description'   => t('Enter width of right sidebar (px)'),
  );
  }}
  
  // HEADER settings
$form['kaprys_header_activate'] = array(

    '#type' 		 => 'checkbox', 
    '#title'         => t('Use uniform layout of header'),
    '#default_value' => theme_get_setting('kaprys_header_activate'),
    '#description'   => t('This feature emulates typical appearance of header of ZHP websites, which works on TYPOlight. After disabling this option in place of the header appears region <em>header</em>'),

);

if(theme_get_setting('kaprys_header_activate') == 1) {
$form['kaprys_header'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Header settings'),
  );

    $form['kaprys_header']['kaprys_logotype'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Path to ZHP logotype'),
    '#default_value' => theme_get_setting('kaprys_logotype'),
    '#description'   => t('If you want to use normal logotype, don`t enter here anything.'),
	'#element_validate'=> array('_kaprys_logotype_validate'),
  );
  
  $form['kaprys_header']['kaprys_top_menu'] = array(
    '#type' => 'select',
    '#title' => t('Source for the Top menu'),
    '#empty_option' => t('No Top menu'),
	'#default_value' => theme_get_setting('kaprys_top_menu'),
    '#options' => menu_get_menus(),
    '#tree' => FALSE,
    '#description' => t('Select what should be displayed as the Top menu.'),
  );
  
  $form['kaprys_header']['kaprys_contact_data'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Contact data'),
    '#default_value' => theme_get_setting('kaprys_contact_data'),
  );
  
  }
//FOOTER settings

$form['kaprys_footer_activate'] = array(

    '#type' 		 => 'checkbox', 
    '#title'         => t('Use uniform content of footer'),
    '#default_value' => theme_get_setting('kaprys_footer_activate'),
    '#description'   => t('This feature emulates typical appearance of footer of ZHP websites, which works on TYPOlight. After disabling this option in place of the footer appears region <em>footer</em>'),  
	);

}

function _kaprys_width_total_validate($element, &$form_state, $form) {
	
	if (strval(intval($element['#value'])) != $element['#value'] || $element['#value'] < 0)
		form_error($element, t('Value of total width is incorrect'));
	elseif ($element['#value'] < 500 || $element['#value'] > 9999)
		form_error($element, t('Total width should be between 500px and 9999px'));
}

function _kaprys_width_left_validate($element, &$form_state, $form) {
	$kaprys_width_total = $form_state['values']['kaprys_width_total'];
	
	if (strval(intval($element['#value'])) != $element['#value'] || $element['#value'] < 0)
		form_error($element, t('Value of width of left sidebar is incorrect'));
	elseif ($element['#value'] < 100 || $element['#value'] > ($kaprys_width_total/2))
		form_error($element, t('Width of the left sidebar cannot be lower than 100px and higher than the half of total width (now !halfpx)', array('!half' => floor($kaprys_width_total/2))));
}
		
function _kaprys_width_right_validate($element, &$form_state, $form) {
//When right sidebar was activated
	if (theme_get_setting('kaprys_right_sidebar') != 0 && $form_state['values']['kaprys_right_sidebar'] != 0)
	{
		$form_state['values']['kaprys_width_right'] == '180';
	}
	elseif ($form_state['values']['kaprys_right_sidebar'] != 0)
	{
	$kaprys_width_total = $form_state['values']['kaprys_width_total'];
	if (strval(intval($element['#value'])) != $element['#value'] || $element['#value'] < 0)
		form_error($element, t('Value of width of right sidebar is incorrect'));
	elseif ($element['#value'] < 100 || $element['#value'] > ($kaprys_width_total/2))
		form_error($element, t('Width of the right sidebar cannot be lower than 100px and higher than the half of total width (now !halfpx)', array('!half' => floor($kaprys_width_total/2))));
	}
}

function _kaprys_logotype_validate($element, &$form_state, $form) {
	
	if (!url_is_external($element['#value']) && $element['#value'] != '')
		form_error($element, t('Logotype path is invalid'));
}