<?php
function kaprys_preprocess_page(&$vars) {
  //Preparing main menu
  $vars['left_menu'] = drupal_render(menu_tree(variable_get('menu_main_links_source', 'main-menu')));
  //Preparing top menu
  if(theme_get_setting('kaprys_top_menu')) {
    $menu_array = menu_navigation_links(theme_get_setting('kaprys_top_menu'));
    foreach($menu_array as $link) {
      $vars['top_menu'] .= '<a href="'.$link['href'].'"';
      if(isset($link['attributes'])) {
        foreach($link['attributes'] as $name => $value) {
          $vars['top_menu'] .= ' '.$name.'="'.$value.'"';
        }
      }
      $vars['top_menu'] .= '>'.$link['title'].'</a>';
    }
  } else
    $vars['top_menu'] = '';
}

function kaprys_preprocess_html(&$vars) {
  //Preparing widths' stylesheet
  $vars['stylesheet'] = 'wrapper {width:'.theme_get_setting('kaprys_top_menu').'px;}';
}
?>